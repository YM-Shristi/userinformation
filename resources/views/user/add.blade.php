@extends('layout.master')

@section('content')
    <section
            class="pb_cover_v3 overflow-hidden cover-bg-indigo cover-bg-opacity text-left pb_gradient_v1"
            id="section-home">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-4">
                    <h1 class="mb-3" style="color: white">{{$greeting}}</h1>

                    <div class="sub-heading">
                        <p class="mb-4">{{gethostname()}}</p>
                        <p class="mb-5">
                            @if(count($users)!=0)
                                <a class="btn btn-success btn-lg pb_btn-pill smoothscroll"
                                           href="{{url('users')}}"><span
                                        class="pb_font-14 text-uppercase pb_letter-spacing-1">View Users</span></a>

                            @endif
                        </p>
                    </div>
                </div>

                <div class="col-md-8 relative align-self-center">

                    <form action="{{route('users.store')}}" class="bg-white rounded pb_form_v1" name="user_form"
                          onsubmit="return validateForm()" method="post">
                        {{csrf_field()}}
                        <h2 class="mb-4 mt-0 text-center">Add New User Information</h2>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control pb_height-50 reverse"
                                           placeholder="Full name" value="{{old('name')}}">
                                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col">

                                <div class="form-group">
                                    <input type="text" name="address" class="form-control pb_height-50 reverse"
                                           placeholder="Address" value="{{old('address')}}">
                                    {!! $errors->first('address', '<span class="text-danger">:message</span>') !!}

                                </div>

                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <input type="text" name="nationality" class="form-control pb_height-50 reverse "
                                         id="nationality"  placeholder="Nationality" value="{{old('nationality')}}">
                                    {!! $errors->first('nationality', '<span class="text-danger">:message</span>') !!}

                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <input type="date" name="dob" id="dob" class="form-control pb_height-50 reverse"
                                                                              placeholder="Date of Birth" value="{{old('dob')}}">
                                    {!! $errors->first('dob', '<span class="text-danger">:message</span>') !!}

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-control reverse">
                                <label>Gender:</label>&nbsp;

                                <input type="radio" class="reverse mp-2 ml-4" name="gender" value="M"<?php if(old('gender')=='M') echo "checked";?> >&nbsp; Male &nbsp;
                                <input type="radio" class="reverse mp-2 ml-4 " name="gender" value="F" <?php if(old('gender')=='F') echo "checked";?>>&nbsp; Female &nbsp;
                                <input type="radio" class="reverse mp-2 ml-4" name="gender" value="O"<?php if(old('gender')=='O') echo "checked";?>>&nbsp; Other


                            </div>
                            {!! $errors->first('gender', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <input type="email" name="email_id" class="form-control pb_height-50 reverse"
                                           placeholder="Email" value="{{old('email_id')}}">
                                    {!! $errors->first('email_id', '<span class="text-danger">:message</span>') !!}

                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control pb_height-50 reverse"
                                           placeholder="Phone Number format 01-5555815" value="{{old('phone')}}">
                                    {!! $errors->first('phone', '<span class="text-danger">:message</span>') !!}

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="pb_select-wrap">
                                <select name="preferred_contact" class="form-control pb_height-50 reverse">
                                    <option value="{{old('preferred_contact')}}" selected>Preferred Contact</option>
                                    <option value="phone">Phone</option>
                                    <option value="email">Email</option>


                                </select>
                                {!! $errors->first('preferred_contact', '<span class="text-danger">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit"
                                   class="btn btn-primary btn-lg btn-block pb_btn-pill  btn-shadow-blue"
                                   value="Register">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
    <!-- END section -->



@endsection
@section('js')

    <script>
        //frontend Validation
        function validateForm() {
            var name = document.forms["user_form"]["name"].value;
            var address = document.forms["user_form"]["address"].value;
            var email_id = document.forms["user_form"]["email_id"].value;
            var phone = document.forms["user_form"]["phone"].value;
            var dob = document.forms["user_form"]["dob"].value;
            if (name == "") {
                alert("Name must be filled out");
                return false;
            }
            if (address == "") {
                alert("Address must be filled out");
                return false;
            }
            if (email_id == "") {
                alert("Email Address must be filled out");
                return false;
            }
            if (phone == "") {
                alert("Phone Number must be filled out");
                return false;
            }
            if (dob == "") {
                alert("Date of Birth must be filled out");
                return false;
            }
        }


    </script>



@endsection