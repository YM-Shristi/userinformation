@extends('layout.master')
@section('content')
    <section class=" pb_section pb_slant-reverse p-5">

        <div class="container rounded pb_form_v1" >


            @if (Session::has('success'))
                <div class="alert alert-success">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row justify-content-center mb-0">
                <div class="col-md-6 text-center mb-2">
                    <h5 class="text-uppercase pb_font-15 mb-2 pb_color-dark-opacity-12 pb_letter-spacing-2"><strong><i class="fa fa-users"></i> User Information</strong>
                    </h5>

                </div>

            </div>

            <div class="row">
                <div class="col-md">
                    <a href="{{url('users/export-csv')}}" class="pull-right p-1 text-success" data-toggle="tooltip" title="Export"><i class="fa fa-file-excel-o fa-2x"></i></a>

                    <a href="{{route('users.create')}}" target="_blank" class="pull-right p-1" data-toggle="tooltip" title="Add Users"><i class="fa fa-plus-circle fa-2x"></i></a>
                    <div  class="pb_accordion" data-children=".item">
                        <table class="table table-responsive table-bordered">
                            <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Full Name/Contact</th>
                                <th>Gender/Date of Birth</th>
                                <th>Address/Nationality</th>

                                <th>Preferred Contact</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key=>$user)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>
                                        <i class="fa fa-user"></i>&nbsp;{{ucfirst($user->name)}}
                                        <br>
                                        <i class="fa fa-envelope"> </i>&nbsp;{{$user->email_id}}<br>
                                        <i class="fa fa-phone"> </i>&nbsp;{{$user->phone}}
                                    </td>
                                    <td>
                                        @if($user->gender=="M")
                                            <i class="fa fa-male"> Male</i>
                                        @elseif($user->gender=="F")
                                            <i class="fa fa-male"> Female</i>

                                        @else
                                            <i class="fa fa-transgender"> Other</i>

                                        @endif
                                        <br>
                                        <i class="fa fa-birthday-cake">  {{$user->dob}}</i>
                                    </td>
                                    <td>
                                     <i class="fa fa-location-arrow"></i>   {{ucfirst($user->address)}}<br>
                                       <i class="fa fa-flag"></i> {{ucfirst($user->nationality)}}
                                    </td>
                                    <td class="text-center">
                                        @if($user->preferred_contact=="phone")
                                            <span class="pb-icon pb_color-primary">  <i
                                                        class=" fa fa-phone-square fa-3x">&nbsp;</i></span>

                                        @else
                                            <span class="pb_color-success">  <i class="fa fa-envelope fa-3x">&nbsp;</i></span>

                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>












    </section>
    <!-- END section -->




@endsection