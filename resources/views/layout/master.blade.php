<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>User Information</title>


    <link rel="apple-touch-icon" sizes="57x57" href="{{url('assets/icon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{url('assets/icon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('assets/icon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('assets/icon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('assets/icon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url('assets/icon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{url('assets/icon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('assets/icon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('assets/icon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{url('assets/icon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('assets/icon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{url('assets/icon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('assets/icon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('assets/icon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <meta name="description" content="User Information">
    <meta name="keywords" content="User Information">

    <link rel="stylesheet" href="{{url('assets/css/bootstrap/bootstrap.css')}}">

    <link rel="stylesheet" href="{{url('assets/fonts/fontawesome/css/font-awesome.min.css')}}">


    <link rel="stylesheet" href="{{url('assets/css/helpers.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
</head>
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">

<!-- END nav -->
@yield('content')

<footer class="pb_footer bg-light" role="contentinfo">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="{{url('/')}}" class="p-2"><i class="fa fa-facebook fa-2x"></i></a></li>
                    <li class="list-inline-item"><a href="{{url('')}}" class="p-2"><i class="fa fa-twitter fa-2x"></i></a></li>
                    <li class="list-inline-item"><a href="{{url('/')}}" class="p-2"><i class="fa fa-linkedin fa-2x"></i></a></li>
                </ul>
            </div>
        </div>

    </div>
</footer>

<script src="{{url('assets/js/bootstrap.min.js')}}"></script>

@yield('js')
</body>
</html>