<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About System

This is simply the demo of the system. System stores the user information provided and display the list of stored users. 


## Installation Guide

##System Requirements

- PHP >= 7.1.3
-  OpenSSL PHP Extension
 - PDO PHP Extension
-  Mbstring PHP Extension
-  Tokenizer PHP Extension
-  XML PHP Extension
-  Ctype PHP Extension
- JSON PHP Extension

- Composer


## Steps
Step -1: Basic Requirements for installing the laravel project
- Install composer in your machine if it is not installed
- Install Apache Server either LAMP, WAMP, XAMP
- Install Database Server (If LAMP, Wamp or XAMP installed, No needed to install separately)
-Install composer if not in the machine

Step-2:
- Update/Install the composer after downloading this project from bitbucket-link https://bitbucket.org/YM-Shristi/userinformation/src/master/
- Command for update the composer => sudo composer update or simply composer update
- Find .env-file in the project if not available then copy .env-example to fresh .env file
- Create Database named userInformation in your database server and provide your database user name in DB_USERNAME and password in DB_PASSWORD
- Then run the project using command php artisan serve

