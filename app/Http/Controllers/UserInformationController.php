<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserInformationRequest;
use App\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class UserInformationController extends Controller
{
    public function index()
    {
        $users = UserInformation::all();
        if (sizeof($users) == 0) {
            return redirect('users/create',compact('users'));
        } else {
            $users = UserInformation::paginate(10);
            return view('user.index', compact('users'));


        }
    }

    public function store(UserInformationRequest $request)
    {
        if ($request->all()) {
            UserInformation::create($request->all());
            session()->flash('success', 'Successfully added user Information');
            return redirect('users');
        }

    }

    public function create()
    {
        $users = UserInformation::all();
        /* Set the $timezone variable to become the current timezone */
        date_default_timezone_set('Asia/Kathmandu');
        /* This sets the $time variable to the current hour in the 24 hour clock format */
        $time = date("H");

        /* If the time is less than 1200 hours, show good morning */
        if ($time < "12") {
            $greeting = "Good morning";
        } else
            /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
            if ($time >= "12" && $time < "17") {
                $greeting = "Good afternoon";
            } else
                /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
                if ($time >= "17") {
                    $greeting = "Good evening";
                }
        return view('user.add', compact('greeting','users'));
    }

    public function generateCsv()
    {
        $headers = array(
            'Content-Type' => 'application/vnd.ms-excel; charset=utf-8',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-Disposition' => 'attachment; filename=abc.csv',
            'Expires' => '0',
            'Pragma' => 'public',
        );

        $filename = "download.csv";
        $handle = fopen($filename, 'w');
        fputcsv($handle, [
            "id",
            "name"
        ]);

        DB::table("user_informations")->orderBy('id','desc')->chunk(100, function ($data) use ($handle) {
            foreach ($data as $row) {
                // Add a new row with data
                fputcsv($handle, [
                    $row->id,
                    $row->name
                ]);
            }
        });


        fclose($handle);

        return Response::download($filename, "download.csv", $headers);
    }
}
