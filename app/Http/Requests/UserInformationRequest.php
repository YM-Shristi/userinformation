<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'address'=>'required',
            'nationality'=>'required',
            'dob'=>'required',
            'phone'=>'required|regex:/(01)(-)[0-9]/',
            'email_id'=>'required',
            'preferred_contact'=>'required',
            'gender'=>'required',

        ];
    }
}
