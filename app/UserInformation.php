<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $fillable=['name','gender','phone','email_id','address','nationality','dob','preferred_contact'];
}
